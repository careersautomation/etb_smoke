package regression.mobilityexchange.tests;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;

import java.io.File;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import junit.framework.Assert;
import pages.mobilityexchange.CartPage;
import pages.mobilityexchange.CheckoutPage;
import pages.mobilityexchange.CustomReportTab;
import pages.mobilityexchange.MFA;
import pages.mobilityexchange.MyDataTab;
import pages.mobilityexchange.OrderDetailsPage;
import pages.mobilityexchange.ProductCatalogPage;
import pages.mobilityportal.ClientSelectionPage;
import pages.mobilityportal.DashboardPage;

import static pages.mobilityportal.ClientSelectionPage.selectCustomer;
import static pages.mobilityportal.DashboardPage.*;
import static pages.mobilityportal.HeaderPage.*;
import static pages.mobilityexchange.CustomReportTab.*; 
import static pages.mobilityexchange.MyDataTab.*; 
import static pages.mobilityexchange.ProductCatalogPage.*;
import static pages.mobilityexchange.CheckoutPage.*;
import static pages.mobilityexchange.OrderDetailsPage.*;

import pages.mobilityportal.LoginPage;
import utilities.FileDownloader;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_GHRM extends InitTests{
	public static WebDriver driver = null;
	public static Driver driverObj;
	public static ExtentTest test = null;
	public static WebDriver webdriver = null;
	
	LoginPage loginObj;
	MFA mfaObj;
	ClientSelectionPage clientSelectionObj;
	DashboardPage dashboardObj;
	CustomReportTab customReportObj;
	MyDataTab myDataObj;
	ProductCatalogPage productCatalogObj;
	CartPage cartObj;
	CheckoutPage checkoutObj;
	OrderDetailsPage orderDetailsObj;
	
	
	public TC1_GHRM(String appName) {
		super(appName);
	}

	
	@BeforeClass
	public void setUp() throws Exception {
		@SuppressWarnings("unused")
		TC1_GHRM obj = new TC1_GHRM("MobilityExchange");
		driverObj = new Driver();
		webdriver = driverObj.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");	
	}

	@Test(priority = 1, enabled = true)
	public void login() throws Exception {
		test = reports.createTest("Verifying Login to ME as GHRM user");
		test.assignCategory("regression");	
		driver = driverObj.getEventDriver(webdriver, test);
		Assert.assertTrue(true);
	
		loginObj = new LoginPage(driver);
		mfaObj = new MFA(driver);
		clientSelectionObj = new ClientSelectionPage(driver);
		dashboardObj = new DashboardPage(driver);
		
		try {
			loginObj.login(USERNAME, PASSWORD);
			if(driver.getPageSource().contains("Verify your identity")){
				mfaObj.authenticate();
			}
			assertTrue(isElementExisting(driver,selectCustomer,100),"Account selection page did not load",test);
			clientSelectionObj.selectCustomer("MERCER");
			clientSelectionObj.selectAccount("97216906 GHRM Clients");
			clientSelectionObj.clickOnContinue();
			waitForElementToDisplay(loggedInAccountNo);
			verifyElementTextContains(loggedInAccountNo,"97216906 GHRM Clients",test);
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
	}
	
	@Test(priority = 2, enabled = true)
	public void applicationLinks() throws Exception {
		try {
			test = reports.createTest("Verifying GHRM links in Dashboard");
			test.assignCategory("smoke");
			driver = driverObj.getEventDriver(webdriver, test);
			
			dashboardObj = new DashboardPage(driver);
			
			dashboardObj.openLink("Cost of Living Allowance Calculator");
			assertTrue(driver.getTitle().contains("Cost of living allowance calculator"),"Cost of Living page failed to load",test);
			((JavascriptExecutor)driver).executeScript("window.close()");
			switchToWindow(driver,"Dashboard");
			dashboardObj.openLink("Quality of Living Report");
			assertTrue(driver.getTitle().contains("Global HRMonitor Reports"),"Quality of Living page failed to load",test);
			((JavascriptExecutor)driver).executeScript("window.close()");
			switchToWindow(driver,"Dashboard");
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
	}
	@Test(priority = 3, enabled = true)
	public void pageNavigations() throws Exception {
		try {
			test = reports.createTest("Verifying Page Navigations for GHRM account");
			test.assignCategory("smoke");
			driver = driverObj.getEventDriver(webdriver, test);
			
			dashboardObj = new DashboardPage(driver);
			
			dashboardObj.header.navigateLink("Solutions","Cost of Living");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Cost Of Living page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Quality of Living");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Quality Of Living page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Housing");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Housing page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Personal Income Tax");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Personal Income Tax page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Business Travel Allowance");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Business Travel Allowance page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Airfare Database");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Airfare Database page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Country Guides");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Mercer Passport page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Long-Term Assignments");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Long-Term Assignments page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Short-Term Assignments");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Short-Term Assignments page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Technology Solutions");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Localized Compensation page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Benchmarking Policies & Practices");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Benchmarking Policies & Practices page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Cultural Training");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Cultural Training page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Starting a Mobility Program");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Starting a Mobility program page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Global Mobility Policies and Practices");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Global Mobility Policies and Practices page failed to load",test);
			dashboardObj.header.navigateLink("Solutions","Technology Solutions");
			assertTrue(isElementExisting(driver,pageContent,5), "Solutions > Technology Solutions page failed to load",test);
			
			dashboardObj.header.navigateLink("Why Mercer","About us");
			assertTrue(isElementExisting(driver,pageContent,5), "Why Mercer > About Us page failed to load",test);
			
			dashboardObj.header.navigateLink("Insights","News");
			assertTrue(isElementExisting(driver,pageContent,5), "Insights > News page failed to load",test);
			
//			dashboardobj.navigate("Insights","Webinars");
//			assertTrue(isElementExisting(driver,dashboardobj.PageContent,5), "Insights > Webinars page failed to load",test);
			
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e,
					driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("in exception before fail");
			SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
	}
	@Test(priority = 4, enabled = true)
	public void dashboardSupportLinks() throws Exception {
		try {
			test = reports.createTest("Verifying Dashboard support links");
			test.assignCategory("smoke");
			driver = driverObj.getEventDriver(webdriver, test);
			
			dashboardObj = new DashboardPage(driver);
			clientSelectionObj = new ClientSelectionPage(driver);

			dashboardObj.header.contactUs();
			assertTrue(isElementExisting(driver,pageContent,5), "Contact Us page failed to load",test);
			dashboardObj.header.dashboard();
			dashboardObj.feedbackLink();
			assertTrue(isElementExisting(driver,feedbackPageHeader,5), "Feedback page failed to load",test);
			((JavascriptExecutor)driver).executeScript("window.close()");
			switchToWindow(driver,"Dashboard");
			dashboardObj.faqsLink();
			assertTrue(isElementExisting(driver,pageContent,5), "FAQ's page failed to load",test);
			dashboardObj.socialShareIcons("linkedin");
			assertTrue(driver.getTitle().contains("LinkedIn"), "LinkedIn share page failed to load",test);
			((JavascriptExecutor)driver).executeScript("window.close()");
			switchToWindow(driver,"Dashboard");
			dashboardObj.socialShareIcons("facebook");
			assertTrue(driver.getTitle().contains("Facebook"), "Facebook share page failed to load",test);
			((JavascriptExecutor)driver).executeScript("window.close()");
			switchToWindow(driver,"Dashboard");
			dashboardObj.socialShareIcons("twitter");
			assertTrue(driver.getTitle().contains("Twitter"), "Twitter share page failed to load",test);
			((JavascriptExecutor)driver).executeScript("window.close()");
			switchToWindow(driver,"Dashboard");
			dashboardObj.header.dashboard();
			dashboardObj.switchClientLink();
			assertTrue(isElementExisting(driver,selectCustomer,20),"Account Selection page failed to load",test);
			clientSelectionObj.selectCustomer("MERCER");
			clientSelectionObj.selectAccount("97216906 GHRM Clients");
			clientSelectionObj.clickOnContinue();
			waitForElementToDisplay(loggedInAccountNo);
}catch (Error e) {
	e.printStackTrace();
	SoftAssertions.fail(e,
			driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
	ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	softAssert.assertAll();

} catch (Exception e) {
	e.printStackTrace();
	System.out.println("in exception before fail");
	SoftAssertions.fail(e,
		driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
	ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	softAssert.assertAll();
}
	}
		@Test(priority = 5, enabled = true)
		public void customReportTab() throws Exception {
			try {
				test = reports.createTest("Creating, adding and deleting files and folders from 'Custom Report' tab");
				test.assignCategory("smoke");
				driver = driverObj.getEventDriver(webdriver, test);
				
				dashboardObj = new DashboardPage(driver);
				customReportObj = new CustomReportTab(driver);
				
				dashboardObj.switchTab("Custom Report");
				customReportObj.createFolder("TestFolder");
				assertTrue(isElementExisting(driver, createFolderNotification , 5),"Folder creation failed",test);
				customReportObj.changeDirectory("TestFolder");
				customReportObj.addFile("samplePicture.png");
				assertTrue(isElementExisting(driver, addFileNotification , 5),"File uploading failed",test);
				customReportObj.deleteFile("samplePicture.png");
				assertTrue(isElementExisting(driver, deleteFileNotification , 5),"File deletion failed",test);
				customReportObj.deleteFolder("TestFolder");
				assertTrue(isElementExisting(driver, deleteFolderNotification , 5),"Folder deletion failed",test);

	}catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
		ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		softAssert.assertAll();

	} catch (Exception e) {
		e.printStackTrace();
		System.out.println("in exception before fail");
		SoftAssertions.fail(e,
			driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
		ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		softAssert.assertAll();
	}
	}
		@Test(priority = 6, enabled = true)
		public void myDataTab() throws Exception {
			try {
				test = reports.createTest("Viewing all the products from 'My Data' tab");
				test.assignCategory("smoke");
				driver = driverObj.getEventDriver(webdriver, test);
				
				dashboardObj = new DashboardPage(driver);
				myDataObj = new MyDataTab(driver);
				
				dashboardObj.switchTab("My Data");
				myDataObj.productSelection("Airfare Database");
//				assertTrue(purchased || available,"Airfare Database products are not available for the user",test);
//				myDataObj.productSelection("Benefits, Policies and Practices Report");
//				assertTrue(purchased || available,"Benefits, Policies and Practices Report products are not available for the user",test);
////				mydataobj.productSelection("Benefits, Policies and Practices Report - China");
////				assertTrue(purchased || available,"Benefits, Policies and Practices Report - China products are not available for the user",test);
////				mydataobj.productSelection("Car prices and Policies");
////				assertTrue(purchased || available,"Car prices and Policies products are not available for the user",test);
//				myDataObj.productSelection("Compensation Overview Report");
//				assertTrue(purchased || available,"Compensation overview Report products are not available for the user",test);
////				mydataobj.productSelection("Compensation overview Report - China");
////				assertTrue(purchased || available,"Compensation overview Report - China products are not available for the user",test);
//				myDataObj.productSelection("Cost Of Living");
//				assertTrue(purchased || available,"Cost Of Living products are not available for the user",test);
////				mydataobj.productSelection("Education Report");
////				assertTrue(purchased || available,"Education Report products are not available for the user",test);
//				myDataObj.productSelection("Housing");
//				assertTrue(purchased || available,"Housing products are not available for the user",test);
//				myDataObj.productSelection("Mercer Passport");
//				assertTrue(purchased || available,"Mercer Passport products are not available for the user",test);
//				myDataObj.productSelection("Per Diem City Report");
//				assertTrue(purchased || available,"Per Diem City Report products are not available for the user",test);
//				myDataObj.productSelection("Personal Income Tax");
//				assertTrue(purchased || available,"Personal Income Tax products are not available for the user",test);
//				myDataObj.productSelection("Quality Of Living");
//				assertTrue(purchased || available,"Quality Of Living products are not available for the user",test);
////				mydataobj.productSelection("Transportation Allowance");
////				assertTrue(purchased || available,"Transportation Allowance products are not available for the user",test);
//				myDataObj.productSelection("Worldwide Benefit and Employment Guidelines");
//				assertTrue(purchased || available,"Worldwide Benefit and Employment Guidelines products are not available for the user",test);
//			
				myDataObj.addToCartFromMyDataPage("Personal Income Tax");
				
	}catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
		ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		softAssert.assertAll();

	} catch (Exception e) {
		e.printStackTrace();
		System.out.println("in exception before fail");
		SoftAssertions.fail(e,
			driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
		ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		softAssert.assertAll();
	}
	}
		@Test(priority = 7, enabled = true)
		public void purchase() throws Exception {
			try {
				test = reports.createTest("Verifying product purchase");
				test.assignCategory("smoke");
				driver = driverObj.getEventDriver(webdriver, test);
				
				productCatalogObj = new ProductCatalogPage(driver);
				cartObj = new CartPage(driver);
				checkoutObj = new CheckoutPage(driver);
				orderDetailsObj = new OrderDetailsPage(driver);
				
				dashboardObj.header.navigateLink("Shop", "Product Catalog");
				assertTrue(driver.getTitle().contains("Product Catalog"),"Product Catalog page failed to load",test);
				productCatalogObj.purchaseProduct("Cost Of Living");
				assertTrue(isElementExisting(driver,addedToCartNotification,20),"Cost of Living products were not added to cart",test);
		        cartObj.continueShopping();
		        assertTrue(driver.getTitle().contains("Product Catalog"),"Product Catalog page failed to load",test);
		        productCatalogObj.purchaseProduct("Personal Income Tax");
		        assertTrue(isElementExisting(driver,addedToCartNotification,20),"Personal Income Tax products were not added to cart",test);
				cartObj.navigateTocheckout();
				verifyElementTextContains(checkoutPageHeader,"Checkout",test);
				checkoutObj.checkBillingDetails();
				checkoutObj.placeOrder();
				verifyElementTextContains(orderCompleteMessage,"Order Received.",test);
				
					waitForElementToDisplay(downloadPDF);
					FileDownloader downloadTestFile = new FileDownloader(driver);
			        String downloadedFileAbsoluteLocation = downloadTestFile.downloadFile(downloadPDF);
			        System.out.println(downloadedFileAbsoluteLocation);
			        assertTrue(new File(downloadedFileAbsoluteLocation).exists(),"File downloaded successfully",test);
			        verifyEquals(downloadTestFile.getHTTPStatusOfLastDownloadAttempt(), 200, test);
			        
			        dashboardObj.header.navigateLink("Shop", "Product Catalog");
			        productCatalogObj.purchaseProduct("Mercer Passport");
					assertTrue(isElementExisting(driver,addedToCartNotification,20),"Mercer Passport products were not added to cart",test);
					cartObj.navigateTocheckout();
					verifyElementTextContains(checkoutPageHeader,"Checkout",test);
					checkoutObj.checkBillingDetails();
					checkoutObj.placeOrder();
					verifyElementTextContains(orderCompleteMessage,"Order Received.",test);
					
					dashboardObj.header.navigateLink("Shop", "Product Catalog");
					productCatalogObj.purchaseProduct("Balance Sheet Calculator");
					assertTrue(isElementExisting(driver,addedToCartNotification,5),"Balance Sheet Calculator product was not added to cart",test);
					cartObj.navigateTocheckout();
					verifyElementTextContains(checkoutPageHeader,"Checkout",test);
					checkoutObj.checkBillingDetails();
					checkoutObj.placeOrder();
					waitForPageLoad(driver);
					verifyElementTextContains(orderCompleteMessage,"Order Received.",test);
					
					dashboardObj.header.navigateLink("Shop", "Order History");
					assertTrue(driver.getTitle().contains("Order History"),"Order History page failed to load",test);
					orderDetailsObj.viewPreviousOrder();
					verifyElementTextContains(orderNumberHeading,"Order",test);
					verifyElementTextContains(orderDateHeading,"Date",test);
					
					dashboardObj.header.logout();
					assertTrue(isElementExisting(driver,homePageSurvey,20), "Logged out successfully",test);
				
	}catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e,
				driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
		ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		softAssert.assertAll();

	} catch (Exception e) {
		e.printStackTrace();
		System.out.println("in exception before fail");
		SoftAssertions.fail(e,
			driverObj.getScreenPath(webdriver, new Exception().getStackTrace()[0].getMethodName()), test);
		ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		softAssert.assertAll();
	}
	}

	
	@AfterClass 
	public void close(){
		reports.flush();
		driver.quit();
	
	}
	
}