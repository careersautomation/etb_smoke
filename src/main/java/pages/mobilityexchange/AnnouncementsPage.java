package pages.mobilityexchange;

import static driverfactory.Driver.clickElement;


import static driverfactory.Driver.*;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AnnouncementsPage {
	WebDriver driver;
	@FindBy(xpath = "//a[contains(text(),'Add New Record')]")
	WebElement addNewRecord;
	@FindBy(xpath = "//label[contains(text(),'Text')]//following::textarea")
	WebElement textInput;
	@FindBy( xpath ="//label[contains(text(),'Start Date')]//following::input[@name='startDate']")
	WebElement startDate;
	@FindBy(xpath = "//label[contains(text(),'End Date')]//following::input[@name='endDate']")
	WebElement endDate;
	@FindBy(xpath ="//label[contains(text(),'Link')]//following::input[@name='link']")
	WebElement link;
	@FindBy(xpath ="//a[contains(text(),'Save')]")
	WebElement save;
	@FindBy(xpath = "//input[@value='Role']")
	WebElement role;
	@FindBy(xpath = "//a[contains(@ng-click,'current + 1')]")
	WebElement nextPage;
	@FindBy(xpath = "//span[contains(text(),'Sitewide Announcement saved successfully')]")
	public static WebElement savedSitewideNotification;
	@FindBy(xpath = "//span[contains(text(),'Sitewide Announcement deleted successfully')]")
	public static WebElement deletedSitewideNotification;
	@FindBy(xpath = "//span[contains(text(),'Targeted Notification saved successfully')]")
	public static WebElement savedTargetedNotification;
	@FindBy(xpath = "//span[contains(text(),'Targeted Notification deleted successfully')]")
	public static WebElement deletedTargetedNotification;
	@FindBy(xpath = "//span[contains(text(),'Survey saved successfully')]")
	public static WebElement savedSurveyNotification;
	@FindBy(xpath = "//span[contains(text(),'Survey deleted successfully')]")
	public static WebElement deletedSurveyNotification;
	
	public AnnouncementsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	public void selectAnnouncementType(String announcement) throws InterruptedException {
		clickElement(driver.findElement(By.xpath("//a[contains(text(),'"+announcement+"')]")));
	}
	public void addNew(String text, String startdate, String enddate) throws InterruptedException {
		clickElement(addNewRecord);
		setInput(textInput,text);
		clickElementUsingJavaScript(driver,this.startDate);
		setInput(this.startDate,startdate);
		clickElementUsingJavaScript(driver,this.endDate);
		setInput(this.endDate,enddate);
		clickElement(save);
	}
	public void addNew(String text, String startdate, String enddate,String role) throws InterruptedException {
		clickElement(addNewRecord);
		setInput(textInput,text);
		clickElementUsingJavaScript(driver,this.startDate);
		setInput(this.startDate,startdate);
		clickElementUsingJavaScript(driver,this.endDate);
		setInput(this.endDate,enddate);
		clickElementUsingJavaScript(driver,this.role);
		if(role == "GHRM" || role == "ICS") {
		clickElement(driver.findElement(By.xpath("//span[text()='"+role+"']//preceding::span[@name='SelectedRoles'][1]")));
		}
		else {
			clickElement(driver.findElement(By.xpath("//span[text()='GHRM']//preceding::span[@name='SelectedRoles'][1]")));
			clickElement(driver.findElement(By.xpath("//span[text()='ICS']//preceding::span[@name='SelectedRoles'][1]")));
		}
		clickElement(save);
	}
	public void addNew(String text,String link, String startdate, String enddate,String role) throws InterruptedException {
		clickElement(addNewRecord);
		setInput(textInput,text);
		setInput(this.link,link);
		clickElementUsingJavaScript(driver,this.startDate);
		setInput(this.startDate,startdate);
		clickElementUsingJavaScript(driver,this.endDate);
		setInput(this.endDate,enddate);
//		clickElementUsingJavaScript(driver,SelectRole);
		if(role == "GHRM" || role == "ICS") {
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//span[text()='"+role+"']//preceding::span[@name='SelectedRoles'][1]")));
		}
		else {
			clickElement(driver.findElement(By.xpath("//span[text()='GHRM']//preceding::span[@name='SelectedRoles'][1]")));
			clickElement(driver.findElement(By.xpath("//span[text()='ICS']//preceding::span[@name='SelectedRoles'][1]")));
		}
		clickElement(save);
	}
	public void updateLink(String text,String url) throws InterruptedException {
		int  flag = 0;
			do{
				try {
				delay(2000);
			clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//td[text()='"+text+"']//following::button[contains(text(),'Edit')]")));
			flag =1;
			}catch(NoSuchElementException e) {
				
			clickElement(nextPage);
			}
				}while(flag==0);
			
		
		setInput(link,url);
		clickElement(save);
	}
	public void delete(String text) throws InterruptedException {
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//td[text()='"+text+"']//following::button[contains(text(),'Delete')]")));
		driver.switchTo().alert().accept();
	}
}
