package services.rest.testcases;
import static utilities.MyExtentReports.reports;
import static utilities.RestAPI.get;
import static verify.SoftAssertions.verifyEquals;

import java.io.IOException;
import java.util.HashMap;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import io.restassured.response.Response;
import utilities.InitTests;
public class TestGetReq extends InitTests
{
	
	@Test()
	public static void testGetReq() throws IOException
	{
		ExtentTest test=null;
		try {
			
			test = reports.createTest("testGetReq");
			test.assignCategory("REST");
			 HashMap<String, String> headers = new HashMap<String, String>();
			headers.put("Accept", "text/html,application/xhtml+xml,application/xml");
			Response response = get("https://marketpricer.us-east-1.dev.awsapp.mercer.com/",null,null,headers);
			System.out.println("message body " + response.getBody().asString());
			System.out.println("status code " + response.getStatusCode());
			verifyEquals(response.getStatusCode(), 200, test);

		} catch (Error e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			reports.flush();
		
		}
		
		
	
	}
	
}
