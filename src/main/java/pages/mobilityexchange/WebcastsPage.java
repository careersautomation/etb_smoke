package pages.mobilityexchange;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebcastsPage {
	WebDriver driver;

	@FindBy(xpath = "//a[contains(text(),'Add Webcast')]")
	WebElement addNewWebcast;
	@FindBy(xpath = "//label[contains(text(),'Header')]//following::input[@id='header']")
	WebElement headerInput;
	@FindBy(xpath ="//a[contains(text(),'Save')]")
	WebElement save;
	@FindBy(xpath ="//tags-input[@name='participants']//child::input")
	WebElement participants;
	@FindBy(xpath ="//input[@name='webcastUrl']")
	WebElement webcastUrl;
	@FindBy(xpath ="//input[@name='date']")
	WebElement date;
	@FindBy(xpath ="//textarea[@name='description']")
	WebElement description;
	@FindBy(xpath = "//span[contains(text(),'Webcast added successfully')]")
	public static WebElement createdWebcastNotification;
	@FindBy(xpath = "//span[contains(text(),'Webcast updated successfully')]")
	public static WebElement updatedWebcastNotification;
	@FindBy(xpath = "//span[contains(text(),'Webcast deleted')]")
	public static WebElement deletedWebcastNotification;

	public WebcastsPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	public void addNew(String title, String date, String participants,String url, String Desc) throws InterruptedException {
		clickElement(addNewWebcast);
		setInput(headerInput, title);
		clickElementUsingJavaScript(driver,this.date);
		setInput(this.date,date);
		setInput(this.participants, participants);
		setInput(description,Desc);
		setInput(webcastUrl,url);
		clickElement(save);
	}
	public void update(String title,String Desc) throws InterruptedException {
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//td[text()='"+title+"']//following::button[contains(text(),'Edit')][1]")));
		setInput(description,Desc);
		clickElement(save);
	}
	public void delete(String title) throws InterruptedException {
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//td[text()='"+title+"']//following::button[contains(text(),'Delete')][1]")));
		driver.switchTo().alert().accept();
	}
}
