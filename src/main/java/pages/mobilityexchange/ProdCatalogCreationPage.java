package pages.mobilityexchange;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class ProdCatalogCreationPage {
	WebDriver driver;
	int row=1;

@FindBy(xpath ="//div[contains(text(),'Products')]//following::select[1]")
WebElement product;
@FindBy(xpath = "//div[contains(text(),'Survey Date')]//following::input[1]")
WebElement surveyDate;
@FindBy(xpath = "//div[contains(text(),'Year')]//following::input[@name='productYear']")
WebElement year;
@FindBy(xpath = "//div[contains(text(),'Year')]//following::select[@name='ProductYear']")
WebElement existingCatalogYear;
@FindBy(xpath = "//div[contains(text(),'Methodology')]//following::select[@name='ProductMethodology'][1]")
WebElement methodology;
@FindBy(xpath = "//div[contains(text(),'Category')]//following::input[@name='ProductCategory'][1]")
WebElement category;
@FindBy(xpath = "//div[contains(text(),'Category')]//following::select[@name='ProductCategory'][1]")
WebElement existingCatalogCategory;
@FindBy(xpath = "//div[contains(text(),'Official Product Name')]//following::input[1]")
WebElement officialProductName;
@FindBy(xpath = "//div[contains(text(),'Default Additional Display Name')]//following::input[1]")
WebElement additionalDisplayName;
@FindBy(xpath = "//div[contains(text(),'Description File Name')]//following::input[1]")
WebElement productDescriptionFileName;
@FindBy(xpath = "//div[contains(text(),'Image File Name')]//following::input[1]")
WebElement imageFileName;
@FindBy(xpath = "//div[contains(text(),'ICS Product Code')]//following::select[1]")
WebElement iCSProductCode;
@FindBy(xpath = "//a[contains(text(),'Add New Price')]")
WebElement addNewPrice;
@FindBy(xpath = "//a[contains(text(),'Add New Location')]")
WebElement addNewLocation;
@FindBy(xpath = "//a[contains(text(),'Add Location')]")
WebElement addLocations;
@FindBy(xpath = "//a[contains(text(),'Create Catalog')]")
WebElement createCatalog;
@FindBy(id = "cbUseExistingData")
WebElement createCatalogUsingExisting;
@FindBy(xpath ="//a[text()='Show Details' and contains(@data-ng-click,'valid')]")
WebElement showDetails;
@FindBy(xpath = "//div[contains(@id,'productcatalog-progress')]")
public static WebElement progressBar;
@FindBy(xpath = "//span[contains(@name,'chkSelectAllLocations')]")
WebElement selectAllLocations;
@FindBy(xpath = "//span[contains(@name,'chkIsPurchasableSelectAll')]")
WebElement selectAllIsPurchasable;
@FindBy(xpath = "//span[contains(@name,'chkIsRestrictedSelectAll')]")
WebElement selectAllIsRestricted;
@FindBy(xpath ="//a[contains(text(),'Continue') and @class='btn btn-primary']")
public static WebElement continueButton;

public ProdCatalogCreationPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
	this.driver = driver;
}

public void createNew(String product, String surveyDate, String year, String methodology ) {
	selEleByVisbleText(this.product,product);
	clickElementUsingJavaScript(driver,this.surveyDate);
	setInput(this.surveyDate,surveyDate);
	setInput(this.year,year);
	selEleByVisbleText(this.methodology,methodology);
}
public void createFromExisting(String product, String surveyDate, String year, String existingCatalogYear, String existingCatalogMethodology, String existingCatalogCategory) throws InterruptedException {
	selEleByVisbleText(this.product,product);
	clickElementUsingJavaScript(driver,this.surveyDate);
	setInput(this.surveyDate,surveyDate);
	setInput(this.year,year);
	clickElementUsingJavaScript(driver,createCatalogUsingExisting);
	selEleByVisbleText(this.existingCatalogYear,existingCatalogYear);
	selEleByVisbleText(this.methodology,existingCatalogMethodology);
	selEleByVisbleText(this.existingCatalogCategory,existingCatalogCategory);
	clickElementUsingJavaScript(driver,showDetails);
}
public void addProductDetails(String category,String productName,String descFileName,String imageFileName, String productCode) {
	setInput(this.category,category);
	setInput(officialProductName, productName);
	setInput(productDescriptionFileName,descFileName);
	setInput(this.imageFileName,imageFileName);
	if(isElementExisting(driver,iCSProductCode,2)==true && iCSProductCode.isEnabled()==true)
	selEleByVisbleText(iCSProductCode,productCode);
}
public void addPrice(String currency,String edition, String price) throws InterruptedException {
	clickElement(addNewPrice);
	selEleByVisbleText(driver.findElement(By.xpath("//tr["+row+"]//select[@name='ProductCurrency']")),currency);
	selEleByVisbleText(driver.findElement(By.xpath("//tr["+row+"]//select[@name='ProductCurrencyEdition']")),edition);
	setInput(driver.findElement(By.xpath("//tr["+row+"]//input[@name='ProductCurrencyEditionPrice']")),price);
	row++;
}
public void addLocations(int numberOfLocations) throws InterruptedException {
	clickElement(addNewLocation);
	for(int i=1;i<=numberOfLocations;i++) {
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//tr[@class='ng-scope']["+i+"]//child::span[contains(@name,'chkLocations')]")));
	}
	clickElement(addLocations);
	
}
public void selectAllLocations() throws InterruptedException {
	clickElement(selectAllLocations);
}
public void selectAllOtherAttributes() throws InterruptedException {
		clickElement(selectAllIsPurchasable);
		clickElement(selectAllIsRestricted);
}
public void createCatalog() throws InterruptedException {
	clickElement(createCatalog);
}
public void close() {
	
}
}
