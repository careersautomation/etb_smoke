package pages.mobilityexchange;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LinksPage {
	WebDriver driver;
	@FindBy(xpath = "//a[contains(text(),'Add Link')]")
	WebElement addNewLink;
	@FindBy(xpath = "//input[@name='internalName']")
	WebElement internalName;
	@FindBy(xpath ="//a[contains(text(),'Save')]")
	WebElement save;
	@FindBy(xpath ="//div[@class='modal-content']//select[@name='legacySystem']")
	WebElement legacySystem;
	@FindBy(xpath ="//select[@name='category']")
	WebElement category;
	@FindBy(xpath ="//input[@name='linkText']")
	WebElement linkText;
	@FindBy(xpath ="//input[@name='link']")
	WebElement link;
	@FindBy(xpath = "//span[contains(text(),'Link saved!')]")
	public static WebElement savedLinkNotification;
	@FindBy(xpath = "//span[contains(text(),'Link has been deleted successfully')]")
	public static WebElement deletedLinkNotification;

	public LinksPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	public void addNew(String title,String linktext, String link,String legacySystem,String category) throws InterruptedException {
		clickElement(addNewLink);
		setInput(internalName, title);
		setInput(linkText,linktext);
		setInput(this.link, link);
		selEleByVisbleText(this.legacySystem,legacySystem);//Training
		selEleByVisbleText(this.category,category);//Asia
		clickElement(save);
	}
	public void update(String linktext,String name) throws InterruptedException {
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//td[text()='"+linktext+"']//following::button[contains(text(),'Edit')][1]")));
		setInput(internalName,name);
		clickElement(save);
	}
	public void delete(String linktext) throws InterruptedException {
		clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//td[text()='"+linktext+"']//following::button[contains(text(),'Delete')][1]")));
		driver.switchTo().alert().accept();
	}
}
